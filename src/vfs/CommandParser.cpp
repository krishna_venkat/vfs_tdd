/**
 * @file CommandParser.cpp
 * @brief 
 * @date 2020-04-19
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#include "CommandParser.h"

#include <iostream>

vfs::CommandParser::CommandParser()
{
    std::cout << "Command Parser created" << std::endl;
}

vfs::CommandParser::~CommandParser()
{
    std::cout << "Command Parser destroyed" << std::endl;
}

void vfs::CommandParser::parseCommand(std::string)
{
    std::cout << "Parsing Command" << std::endl;
}