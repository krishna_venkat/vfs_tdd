/**
 * @file CommandParser.h
 * @brief 
 * @date 2020-04-19
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#ifndef VFS_COMMAND_PARSER_H
#define VFS_COMMAND_PARSER_H

#include <iostream>
#include <memory>
#include <vector>

/**
 * @brief Command Parser Interface
 * 
 */
namespace vfs
{
class ICommandParser
{
public:
    virtual void parseCommand(std::string command) = 0;
};

/**
 * @brief Command Parser concrete class which takes in the command as a string and does the following.
 *          1) Parse the command into a vector of strings.
 *          2) Executes the command.
 * 
 */

class CommandParser : public ICommandParser
{
private:
public:
    CommandParser();
    ~CommandParser();

    void parseCommand(std::string command) override;
};
}   // namespace vfs
#endif